provider "aws" {
  profile = var.admin_profile
  region  = "us-west-2"
  alias   = "admin"
}


provider "mongodbatlas" {
  public_key  = var.atlas_public_key
  private_key = var.atlas_private_key
}
