# aws-mongodb-atlas-connection

AWS + MongoDB Atlas + Lambda + VPC Endpoint


## Description

- Set up VPC endpoint for Secrets Manager that is accessible from a provided Lambda
- Set up VPC endpoint for MongoDB Atlas that is accessible from a provided Lambda
- Set up API Gateway with Token-based Authentication using an API Gateway Authorizer.

## Set Up

* set credential as environment variables

```bash
export TF_VAR_atlas_private_key=
export TF_VAR_atlas_public_key=
export AWS_ACCESS_KEY_ID=
export AWS_SECRET_ACCESS_KEY=
export AWS_DEFAULT_REGION="us-west-2"
```
* test run

```bash
terraform apply -var-file="testing.tfvars"
```

* Technical Details

1. mongodbatlas_network_peering.cluster-partner-network-peering : [AWS VPC CIDR block or subset. Must not overlap with your Atlas CIDR Block or any other Network Peering connection VPC CIDR](https://docs.atlas.mongodb.com/security-vpc-peering/)

2. For each interface endpoint, you can choose only one subnet per Availability Zone (https://docs.aws.amazon.com/vpc/latest/userguide/vpce-interface.html#vpce-private-dns)

## Test

Spin up a EC2 Instance within the VPC, within security group

* [Test Secret Manger Endpoint](https://docs.aws.amazon.com/vpc/latest/userguide/vpce-interface.html#describe-interface-endpoint) (for example:)

```bash
aws secretsmanager list-secrets
```

* [Test MongoDB Cluster](https://medium.com/faun/vpc-peering-between-mongodb-atlas-and-aws-with-terraform-a-complete-guide-e49e8c138e10)

```python
import pymongo
client = pymongo.MongoClient("mongodb+srv://mongo_user:<password>@cluster-partner-pl-0.dyclq.mongodb.net/<dbname>?retryWrites=true&w=majority")
db = client.test
```

## Reference

* Set up a Network Peering Connection: https://docs.atlas.mongodb.com/security-vpc-peering/
* Set up a Private Endpoint: https://docs.atlas.mongodb.com/security-private-endpoint/
* MongoDB Atlas Provider: https://registry.terraform.io/providers/mongodb/mongodbatlas/latest/docs
* Use API Gateway Lambda authorizers: https://docs.aws.amazon.com/apigateway/latest/developerguide/apigateway-use-lambda-authorizer.html
* Automating MongoDB Atlas VPC Peering with Terraform: https://www.youtube.com/watch?v=PBa2uj4TG4I
* VPC Peering between MongoDB ATLAS and AWS with Terraform — A Complete Guide: https://medium.com/faun/vpc-peering-between-mongodb-atlas-and-aws-with-terraform-a-complete-guide-e49e8c138e10