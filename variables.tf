// MongoDB atlas
variable atlas_private_key {}
variable atlas_public_key {}
variable atlas_region {}
variable atlas_org_id {}
variable atlas_db_user {}
variable atlas_db_vpc_cidr {}


// AWS
variable aws_vpc_cidr {}
variable stage_name {}
variable aws_region {}
variable "admin_profile" {
  type = string
  default = "superadmin"
}
