package main

import (
	"context"
	"fmt"
	"errors"
	"time"
	"encoding/json"
	"net/http"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-lambda-go/events"
	"github.com/sparrc/go-ping"
	"github.com/aws/aws-sdk-go/service/secretsmanager"	
	"go.mongodb.org/mongo-driver/mongo"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)


type MongoCreds struct {
	ConnectionStrings []map[string]interface{} `json:"connection_strings"`
	Password          string   `json:"password"`
}

var MainRegion = "us-west-2"

func HandleRequest(ctx context.Context, req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	fmt.Println("we are pinging")
	pinger, err := ping.NewPinger("www.google.com")
	if err != nil {
		panic(err)
	}

	pinger.Count = 3
	pinger.Run() // blocks until finished
	stats := pinger.Statistics() // get send/receive/rtt stats
	fmt.Println(stats)
	fmt.Println("connecting to mongo")
	err = ConnectToMongoClient()
	if err != nil {
		fmt.Println("failure to connect to mongo")
	}
	bk := map[string]bool{"ok":true}
	js, err := json.Marshal(bk)
	return events.APIGatewayProxyResponse{
        StatusCode: http.StatusOK,
        Body:       string(js),
    }, err
}

func ConnectToMongoClient() error {
	sess := session.Must(session.NewSession(&aws.Config{
		Region: aws.String(MainRegion),
	}))

	svc := secretsmanager.New(sess)
	input := &secretsmanager.GetSecretValueInput{
		SecretId: aws.String("mongo-access"),
	}
	fmt.Println("getting credentials")

	secret, err := svc.GetSecretValue(input)
	if err != nil {
		return err
	}
	var mongo_creds MongoCreds
	secretJson := []byte(*secret.SecretString)
	err = json.Unmarshal(secretJson, &mongo_creds)
	fmt.Println("credentials fetched")
	fmt.Println(mongo_creds)
	if err != nil {
		return err
	}
	var mongoURI string 
	for _, connection := range(mongo_creds.ConnectionStrings) {
		if val, ok := connection["standard_srv"]; ok {
			mongoURI = val.(string)
		}
	}
	if mongoURI == "" {
		return errors.New("Unable to parse a connecting string from secret")
	}
	clientOptions := options.Client().ApplyURI(mongoURI).SetAuth(options.Credential{Username: "mongo_user", Password: mongo_creds.Password})
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	client, err := mongo.Connect(ctx, clientOptions)
	fmt.Println("connecting")
	if err != nil {
		fmt.Println(err.Error())
		return err
	}

	ctx, _ = context.WithTimeout(context.Background(), 5*time.Second)
	if err = client.Ping(ctx, readpref.Primary()); err != nil {
		return err
	}
	return err
}

func main() {
	lambda.Start(HandleRequest)
}
