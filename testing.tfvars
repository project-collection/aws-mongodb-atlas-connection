# MongoDB atlas
atlas_region="US_WEST_2"
atlas_org_id="5f4aa6c894c1456e2274fe7a"
atlas_db_user="mongo_user"
atlas_db_vpc_cidr="192.168.224.0/21"

# AWS
# always make sure that our AWS VPC CIDR shouldn’t overlap with the ATLAS VPC CIDR.
aws_vpc_cidr="10.2.0.0/16"
stage_name="dev"
aws_region="us-west-2"