// create a vpc in AWS
module "primary-vpc" {
  source = "terraform-aws-modules/vpc/aws"
  name = "corsell"
  cidr = var.aws_vpc_cidr
  azs = ["${var.aws_region}a", "${var.aws_region}b",   "${var.aws_region}c"]
  private_subnets = ["10.2.1.0/24"]
  public_subnets = ["10.2.2.0/24"]
  enable_nat_gateway = false
  enable_vpn_gateway = false
  enable_dhcp_options = true
  dhcp_options_domain_name_servers = ["AmazonProvidedDNS"]
  enable_dns_hostnames = true
}




resource "aws_route" "primary-internet_access" {
  provider               = aws.admin
  route_table_id         = module.primary-vpc.vpc_main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = module.primary-vpc.igw_id
}

resource "aws_route" "peeraccess" {
  provider                  = aws.admin
  route_table_id            = module.primary-vpc.vpc_main_route_table_id
  destination_cidr_block    = var.atlas_db_vpc_cidr
  vpc_peering_connection_id = mongodbatlas_network_peering.cluster-partner-network-peering.connection_id
  depends_on                = [aws_vpc_peering_connection_accepter.peer]
}





// security groups for mongo vpc connect
resource "aws_security_group" "primary_default" {
  provider    = aws.admin
  name_prefix = "defaultvpc-"
  description = "Default security group for all instances in VPC ${module.primary-vpc.vpc_id}"
  vpc_id      = module.primary-vpc.vpc_id
  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    cidr_blocks = [
      module.primary-vpc.vpc_cidr_block,
      var.atlas_db_vpc_cidr
    ]
    # cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


// aws secretsmanager endpoints
resource "aws_vpc_endpoint" "mongocredentials" {
  provider         = aws.admin
  vpc_id            = module.primary-vpc.vpc_id
  service_name      = "com.amazonaws.${var.aws_region}.secretsmanager"
  vpc_endpoint_type = "Interface"

  security_group_ids = [
    aws_security_group.primary_default.id,
  ]
  subnet_ids = module.primary-vpc.public_subnets
  private_dns_enabled = true
}

# mongodb private endpoint
resource "mongodbatlas_private_endpoint" "test" {
  project_id    = mongodbatlas_project.cluster-partner-project.id
  provider_name = "AWS"
  region        = var.aws_region
}

resource "aws_vpc_endpoint" "ptfe_service" {
  vpc_id             = module.primary-vpc.vpc_id
  service_name       = mongodbatlas_private_endpoint.test.endpoint_service_name
  vpc_endpoint_type  = "Interface"
  subnet_ids         = module.primary-vpc.public_subnets
  security_group_ids = [aws_security_group.primary_default.id]
}

resource "mongodbatlas_private_endpoint_interface_link" "test" {
  project_id            = mongodbatlas_private_endpoint.test.project_id
  private_link_id       = mongodbatlas_private_endpoint.test.private_link_id
  interface_endpoint_id = aws_vpc_endpoint.ptfe_service.id
}

